FROM python:3.7-slim

RUN mkdir -p /home/asjaiswal/ags-contracts_table_model

COPY .  /home/asjaiswal/ags-contracts_table_model/


WORKDIR /home/asjaiswal/ags-contracts_table_model

RUN pip install lexnlp==1.8.0

RUN pip install -r requirements.txt

RUN python -m nltk.downloader popular

#ENV GOOGLE_APPLICATION_CREDENTIALS= credential_file_location

ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64/

RUN apt update -y && apt-get install -y software-properties-common && \
    apt-add-repository 'deb http://security.debian.org/debian-security stretch/updates main' && apt update -y && \
    apt-get install -y openjdk-11-jdk-headless && \
#    pip install --no-cache-dir -r requirements.txt && \
    export JAVA_HOME && \
    apt-get clean;

	

#0.9.1
#RUN apt-get install -y curl  && \
#	curl -Lo tabula.jar https://github.com/tabulapdf/tabula-java/releases/download/tabula-0.9.1/tabula-0.9.1-SNAPSHOT-jar-with-dependencies.jar

RUN apt-get install -y curl  && \
    curl -Lo tabula.jar https://github.com/tabulapdf/tabula-java/releases/tag/v1.0.5/tabula-1.0.5-jar-with-dependencies.jar


EXPOSE 5000

CMD exec gunicorn --bind 0.0.0.0:5000 app:app --timeout 10000