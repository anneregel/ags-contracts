import os
import string
import logging

import docx2txt
import spacy
import PyPDF4

from preprocessing.documents_to_text import *
from postprocessing.clean_dollars_dates import *
from postprocessing.utils import company_condition, extract_sow_num_regex
from postprocessing.extract_using_lex import extract_maxbudget_from_lex, extract_buyer_from_lex
from postprocessing.payment_term import extract_payment_term

log = logging.getLogger('flaskapp')


class contract_extract:
    def __init__(self, model_path):
        '''
        This is the constructor method for the contract extract class

        args:
            model_path: the path to the spacy model to be loaded

        returns: None
        '''
        if not os.path.exists(model_path):
            raise Exception("model path does not exist")
        try:
            self.nlp = spacy.load(model_path)
            log.info("model succesfully loaded")
        except:
            raise Exception("spacy unable to load the model. Please verify the model is not corrupt for spacy V2.3")

        self.text = None

    def predict(self, doc_file):
        '''
        this method extracts the SOW related entities from the contract document supplied
        args:
            doc_file: the path to the contract document

        returns: the extracted entities from the list below as a tuple. Any entity that has not been extracted will have None value

        list of entities:		[sow_num, sow_name, agreement_num, buyer, seller, start_date,
                                start_date_cleaned, end_date, end_date_cleaned, sow_duration,
                                max_budget, currency]
        '''
        log.info("called predict method")
        try:
            if doc_file.endswith(".pdf"):
                self.text = extract_pdf(doc_file)
            elif doc_file.endswith(".docx"):
                self.text = extract_docx(doc_file)
            else:
                self.text = ""
        except Exception as e:
            self.text = ""
            log.info(e)

        doc = self.nlp(self.text)

        start_date = None
        start_date_cleaned = None
        buyer = None
        seller = None
        end_date = None
        end_date_cleaned = None
        max_budget = None
        max_budget_cleaned = None
        sow_num = None
        sow_name = None
        agreement_num = None
        sow_duration = None
        currency = None
        buyer_spc = None
        max_budget_spc = None
        sow_num_spc = None
        parties = []
        payment_term = None

        for ent in doc.ents:

            if ent.label_ == 'parties' and company_condition(ent.text):
                buyer = ent.text
                buyer_spc = True
            if ent.label_ == 'parties' and not company_condition(ent.text):
                seller = ent.text
            if ent.label_ == 'parties':
                parties.append(ent.text)
            if ent.label_ == 'start_date':
                start_date = ent.text
                start_date_cleaned = clean_dates(start_date)

            if ent.label_ == 'end_date':
                end_date = ent.text
                end_date_cleaned = clean_dates(end_date)

            if ent.label_ == 'max_budget':
                max_budget = ent.text
                max_budget_cleaned = extract_amount(max_budget, from_ner=True)
                max_budget_spc = True

            if ent.label_ == 'sow_num':
                sow_num = ent.text
                sow_num_spc = True

            if ent.label_ == 'sow_duration':
                sow_duration = ent.text

            if ent.label_ == 'agreement_num':
                agreement_num = ent.text

        if not buyer_spc:
            buyer = extract_buyer_from_lex(self.text)

        if not max_budget_spc:
            max_budget_cleaned = extract_maxbudget_from_lex(self.text)

        if not sow_num_spc:
            sow_num = extract_sow_num_regex(self.text)

        sow_name = doc_file.split(os.path.sep)[-1]
        sow_name = sow_name.split('.')[0]

        if max_budget:
            currency = extract_currency(self.text)

        else:
            currency = None

        payment_term = extract_payment_term(self.text)

        result = (sow_num, sow_name, agreement_num, parties, buyer, seller, start_date,
                  start_date_cleaned, end_date, end_date_cleaned, sow_duration,
                  max_budget, max_budget_cleaned, currency, payment_term)
        log.info("exited predict method")
        return result

    def create_dataframe(self,result):
        '''
        creates an excel sheet for that particular file with its result dataframe.
        input params:= result=> output of the file from predict function
        return dataframe
        '''
        df=pd.DataFrame(columns=['Resource Role','Resource Rate','Unit of Measure','Resource Total Count','Resource Total Cost'])
        tmp = [x for x in result if x != []]
        result = tmp if tmp != [] else [()]
        try:
            for i in result:
                resource_dict = {'Resource Role': [''], 'Resource Rate': [''], 'Unit of Measure': [''],
                                 'Resource Total Count': [''], 'Resource Total Cost': ['']}
                for j in i:
                    if j[1] == 'resource_role':
                        resource_dict['Resource Role'] = [j[0]]
                    elif j[1] == 'resource_rate':
                        resource_dict['Resource Rate'] = [j[0]]
                    elif j[1] == 'resource_unit_of_measure':
                        resource_dict['Unit of Measure'] = [j[0]]
                    elif j[1] == 'resource_total_cost':
                        resource_dict['Resource Total Cost'] = [j[0]]
                    elif j[1] == 'resource_count':
                        resource_dict['Resource Total Count'] = [j[0]]
                if (all(list(set([x==[''] for x in list(resource_dict.values())])))):
                    continue
                else:
                    row=pd.DataFrame.from_dict(resource_dict)
                    df=df.append(row).reset_index(drop=True)
            
            return df
        except Exception as e:
            df=pd.DataFrame(data={"Error":["Error "+str(e)+" occured"]})
            return df

    def table_predict(self, pdf):
        '''
        takes a pdf, and returns the label and labeled text as a list of tuples

        generic example:

                    predict(pdf) ---->
                    [[("software eng.", "resource_role"), ("$65", "resource_rate")],
                     [("product manager", "resource_role"), ("1", "resource_count")],
                     [("software eng.", "resource_role"), ("$65", "resource_rate")]...

                     ]
        '''

        table_data = preprocess_pdf(pdf)
        results = []
        for txt in table_data:
            doc = self.nlp(txt)
            result = ([(ent.text, ent.label_) for ent in doc.ents])
            results.append(result)
        # collate results into a list
        result_lst = results
        log.info("out of predict:",result_lst)
        return result_lst