import re

def extract_payment_term(content: str) -> str:
    '''
    this method extracts payment terms from the content of the contract using regular expression
    input_parameters:
        content: the content of the contracts

    returns: payment term if it exists else None
    '''
    regex_payment_term = r"(?:eoap ?|net ?)[0-9][0-9]?[0-9]?"
    
    # replacing common ocr mistakes with corresponding numbers
    payment_term = re.findall(regex_payment_term, content, flags = re.IGNORECASE)

    if payment_term:
        return payment_term[0]
    else:
        return None