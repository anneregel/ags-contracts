import re
from collections import Counter

import lexnlp.extract.en.constraints
import lexnlp.extract.en.entities.nltk_re

from .utils import company_condition
from .utils import has_number
from .clean_dollars_dates import extract_amount, extract_currency


def extract_maxbudget_from_lex(content: str) -> str:
    '''
    this method is used for 
    '''
    constraints = list(lexnlp.extract.en.constraints.get_constraints(content))
    constraints = [item[1] for item in constraints if might_have_max_budget(item[1])]# if isinstance(item, str)]

    if constraints: 
        text = constraints[0]
        text=text.lower().replace(",", "").replace(" ", "")
        max_budget = extract_amount(text)
        return max_budget
    
    return None

def extract_buyer_from_lex(content: str) -> str:
    '''
    this method
    '''
    companies = list(lexnlp.extract.en.entities.nltk_re.get_companies(content))
    companies = [item.name.lower() for item in companies if company_condition(item.name.lower())]# if isinstance(item, str)]
    lengths = [len(item) for item in companies]
    if lengths:
        seller = companies[lengths.index(max(lengths))]
#         seller = Counter(companies).most_common(1)[0][0]
        return seller
    return None


def might_have_max_budget(text: str) -> bool:
    '''
    this method is used for the le
    '''
    text=text.lower().replace(" ", "")
    if 'minimum' in text or 'limit' in text or 'liability' in text:
        return False
    if not '$' in text:
        return False

    if not has_number(text):
        return False
    return True