import re

from .utils import has_number




def extract_currency(text: str) -> str:
    '''
    this method extracts currency from entities like max_budget
    
    parameter text: entity that has been extracted as max_budget
    returns the currency of the string
    
    #TODO: ask business for examples of non-USD contracts they deal with
   for CAD, it is impossible to go looking for presence of the word "CAD" as "CAD" is used in SOW
   to refer to Computer Aided Design as well
   
   currently only one example of a contract in canadian dollars
    '''

    if 'CAD$' in text:
        return "CAD"
    if re.findall(r'([a-zA-Z]{3})\/(hr|wk)', text):
        curr = re.search(r"([a-zA-Z]{3})\/(hr|wk)", text).groups()[0]
        if curr.lower() != "per":
            return curr.upper()
    return "USD"

def extract_amount(max_budget: str, from_ner: bool = False) -> float:
    '''
    this method extracts the monetary amounts from the max_budget entity
    OCR sometimes gets confused with '0'/'o' and 'l'/'1', this method aims to correct that as well
    
    inputs:
        max_budget: the entity max_budget as extracted from the contract
        from_ner:  boolean indicator to specify if the entity max_budget came from the NER model
        
    returns:
        the cleaned up dollar amount as a float value
    '''
    if from_ner:
        # if from NER model, max_budget is probably more accurate
        max_budget = max_budget.replace(" ", "").replace("$", "").replace(",", "").replace("o", "0").replace("l", "1")            
    else:
        # if extracting using linguistic approaches, no need to replace o with 0 and the like
        max_budget=max_budget.lower().replace(",", "").replace(" ", "").replace("$", "")
        
    #assumption made that dollars in contracts are specified to cents
    dollar_amounts = re.findall('([0-9]+\.[0-9]+)', max_budget)
    dollar_amounts = dollar_amounts if dollar_amounts else  re.findall('([0-9]+)', max_budget)
    
    if dollar_amounts:
        dollar_amounts = max([float(dollar_amount) for dollar_amount in dollar_amounts])
    
    #if from_ner and has_number(max_budget):
        #dollar_amounts = float("".join([ch for ch in max_budget if ch in "0123456789."]))
        
    return dollar_amounts
    
def clean_dates(text: str)-> str:
    '''
    this method cleans extracted dates into the format mm/dd/yyyy
    
    parameter text: the string date to be cleaned
    
    returns the cleaned date in mm/dd/yyyy as a string
    
    '''
    months, dates, years = None, None, None
    
    ent_text = text.lower().replace(" ","")
    
    #regex for dd/mm/yyyy format
    match_date_format = r"\d{1,2}\/\d{1,2}\/\d{4}"
    
    #regex for dd-mm-yyyy format commonly used in European countries
    match_dd_mm_yyyy = r"\d{1,2}-\d{1,2}-\d{4}"
    
    #regex for mm/dd/yy format
    match_mm_dd_yy = r"\d{1,2}\/\d{1,2}\/\d{2}"
    
    
    if re.findall(match_date_format, ent_text):
        months, dates, years = re.search(r"(\d{1,2})\/(\d{1,2})\/(\d{4})", ent_text).groups()
        
    elif re.findall(match_dd_mm_yyyy, ent_text):
        dates, months, years = re.search(r"(\d{1,2})-(\d{1,2})-(\d{4})", ent_text).groups()
        
    elif re.findall(match_mm_dd_yy, ent_text):
        months, dates, years = re.search(r"(\d{1,2})\/(\d{1,2})\/(\d{2})", ent_text).groups()
        years="20"+years
        
    else:
        years = re.findall(r'20\d{2}', ent_text)
        if years:
            years = years[-1]
            ent_text=ent_text.replace(years, "")
            months = extract_text_month(ent_text)
            if re.findall(r"(\d{1,2})", ent_text):
                dates = re.search(r"(\d{1,2})", ent_text).groups()[0]

        else:
            years=None

    #if date is not specified, assume it is the first of month
    if months and years and not dates: 
        dates = 1
    
    #because of OCR output, if date comes out to greater than 31, change that to 28
    if dates and int(dates)<1 and int(dates)>31:
        dates = 28
    
    if years and months:
        return f"{months}/{dates}/{years}"
    else:
        return None
		
def extract_text_month(text: str)-> int:
    '''
    this method returns the ordinal month based on the presence of a month's name in a text
    
    parameter text: the string from which to extract a numeric month
    
    returns: integer representation of a month
    '''
    text = text.lower()
    month_dict = {"jan": 1,"feb": 2,"mar": 3,"apr": 4,"may": 5, "mny": 5, "jun": 6,"jul": 7,"ju1": 7, "aug": 8,
              "sep": 9, "oct": 10, "0ct": 10,"nov": 11, "n0v": 11, "dec": 12 
             }
    for mon in list(month_dict.keys()):
    
        if mon in text:
            return month_dict[mon]
        
    return None
 

        

if __name__=="__main__":
    #todo: add test cases
    pass