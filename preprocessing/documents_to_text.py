import PyPDF4
import os
import re
import docx2txt
from google.cloud import storage


def write_file(file_name, content):
    '''
    writes a text file with utf-8 encoding given the content of the text file
    parameter file_name: the filename to write the contents into eg. /path/to/file.txt
    parameter content: the contents to write into the text file
    '''
    with open(file_name, 'w', encoding = 'utf-8') as text_file:
        text_file.write(content)
    return None


def clean_string(string_):
    '''
    removes repeating white space character
    TODO: removes gibberish characters
    parameter string_ : string that needs to be cleaned up for writing to text file for labeling
    '''
    string_ = re.sub('\s+',' ',string_)

    return string_

def extract_pdf(pdf_path, gcs_path = False):
    '''
    extracts text contents of a pdf file and returns it as a string
    parameter pdf_path : the path to the pdf to be read
    returns: the extracted text
    '''
    if not gcs_path:
        separator = os.path.sep
        text = f"filename: {pdf_path.split(separator)[-1]} "
        with open(pdf_path, 'rb') as pdfFileObj:

            pdfReader = PyPDF4.PdfFileReader(pdfFileObj) 

            try:
                for page in range(pdfReader.numPages):
                    pageObj = pdfReader.getPage(page) 	
                    text += pageObj.extractText()
            except Exception as e:
                text += ""
                return text

        text = " ".join([line.rstrip() for line in text.splitlines() if line.strip()])
        text = clean_string(text)
        
    else:
        separator = os.path.sep
        filename = pdf_path.split(separator)[-1]
        text = f"filename: {filename} "
        client = storage.Client()
        bucket = pdf_path.split("/")[2]
        prefix = "/".join(pdf_path.split("/")[3:])
        bucket = client.get_bucket(bucket)
        blob = storage.Blob(prefix, bucket)
        pdfFileObj = blob.download_to_filename('/tmp/'+filename)
        
        with open('/tmp/'+filename, 'rb') as pdfFileObj:

            pdfReader = PyPDF4.PdfFileReader(pdfFileObj) 

            try:
                for page in range(pdfReader.numPages):
                    pageObj = pdfReader.getPage(page) 	
                    text += pageObj.extractText()
            except Exception as e:
                text += ""
                return text

        text = " ".join([line.rstrip() for line in text.splitlines() if line.strip()])
        text = clean_string(text)
        os.remove('/tmp/'+filename)

    return text

def extract_docx(docx_file):
    '''
    extracts text contents of a docx file and returns it as a string
    parameter docx_path : the path to the docx file to be read
    '''
    separator = os.path.sep
    text = f"filename: {docx_file.split(separator)[-1]} "

    try:
        text += docx2txt.process(docx_file)
    except Exception as e:
        text += e
        return text

    text = " ".join([line.rstrip() for line in text.splitlines() if line.strip()])
    text = clean_string(text)

    return text

if __name__=='__main__':
    PATH_PDFS = r'C:\Users\pbaral\OneDrive - ALLEGIS GROUP\Documents\AGS_docushift\PDF'
    path_of_output_txt = r'C:\Users\pbaral\OneDrive - ALLEGIS GROUP\Documents\AGS_docushift\Contract Examples\to_label'

    for i, file in enumerate(os.listdir(PATH_PDFS)):
        if file.endswith(".pdf"):
            text=extract_pdf(os.path.join(PATH_PDFS, file))
            write_file(os.path.join(path_of_output_txt, file.split(".")[0]+"_"+".txt"), text)

        elif file.endswith(".docx"):
            text = extract_docx(os.path.join(PATH_PDFS, file))
            write_file(os.path.join(path_of_output_txt, file.split(".")[0]+"_"+".txt"), text)
