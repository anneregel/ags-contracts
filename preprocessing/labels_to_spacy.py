import os
import json

from google.cloud import storage


def download_from_gcs(gcs_path: str, destination_path: str, storage_client: str = 'acs-is-dsia-dev') -> None:
    '''
    this method takes in a jsonl file from an 'entity extraction' dataset
    that has been exported to google cloud storage and saves it in the specified path
    
    input parameters:
        gs_path: the path to the jsonl file in the format - gs://project/folder1/folder2/jsonl_file.jsonl
        
        destination_path: the local path you want to download the jsonl file into eg. /path/to/local/json_file.jsonl
        
        storage_client : the project under which the GCS bucket has been hosted. 
                         For DSIA work, the default value is the DSIA dev project
        
    returns None
    '''
    sep = os.path.sep
   
    storage_client = storage.Client(storage_client)
    bucket_ = gcs_path.split('/')[2]
    if len(gcs_path.split("/"))>3:
        prefix = "/".join(gcs_path.split("/")[3:])
    else:
        raise Exception("please check your gcs_path and make sure a jsonl file exists there")

    bucket = storage_client.get_bucket(bucket_)
    blob = bucket.blob(prefix)
    blob.download_to_filename(destination_path)
    
    return None

def read_text_from_gcs(gcs_path: str, storage_client: str = 'acs-is-dsia-dev') -> str:
    '''
    this method reads text file from a gcs storage URI and returns the content as a string
    
    input_parameters:
        gcs_path: the path to the txt file in the format - gs://bucket/folder1/folder2/text_file.txt
        
        storage_client: the project under which the GCS bucket has been hosted. 
                         For DSIA work, the default value is the DSIA dev project
    
    returns:
        the content of the text file as string
    '''
    storage_client = storage.Client(storage_client)
    
    bucket_ = gcs_path.split('/')[2]
    
    if len(gcs_path.split("/"))>3:
        prefix = "/".join(gcs_path.split("/")[3:])
    else:
        raise Exception("please check your gcs_path and make sure a text file exists there")

    bucket = storage_client.get_bucket(bucket_)
    blob = bucket.blob(prefix)
    downloaded_blob = blob.download_as_string()
    downloaded_blob = downloaded_blob.decode("utf-8") 
    
    return downloaded_blob

def convert_gcs_to_spacy(jsonl_file: str, gcs_path: bool = False) -> list(tuple()):
    '''
    this method takes in a locally saved jsonl_file from GCS dataset labeling and returns the labels in spacy format
    
    input_parameters:
        jsonl_file: the path to the google cloud generated jsonl file. Can be either local path or GCS URI
        gcs_path:   if json_file is a gcs URI, set gcs_path = True
        
    returns:
        the labelled dataset in spacy format 
    
    '''
    TRAIN_DATA = []
    
    # if jsonl_file is a GCS URI, download it first
    if gcs_path:
        print("downloading from gcs")
        download_from_gcs(gcs_path = jsonl_file, destination_path = "gcs-jsonl.jsonl")
        jsonl_file = 'gcs-jsonl.jsonl'
        
    # read the jsonl_file
    with open(jsonl_file) as jsonl:
        jsonl_contents = jsonl.readlines()
        
    #if file is not blank
    if jsonl_contents:
        for i, item in enumerate(jsonl_contents):
            item = json.loads(item)
            if item['textSegmentAnnotations']:
                annotations = item['textSegmentAnnotations']
                gcs_uri = item["textGcsUri"]
                text = read_text_from_gcs(gcs_uri)
                spacy_line = (text, 
                              {"entities": [(annotation["startOffset"], 
                                             annotation["endOffset"], 
                                             annotation["displayName"])
                                            for annotation in annotations] })

                TRAIN_DATA.append(spacy_line)
                
    return TRAIN_DATA  

def convert_doccano_to_spacy(jsonl_file: str, phase_i: bool) -> list(tuple()):
    '''
    this method takes in a jsonl_file from doccano and returns the labelled contracts in spacy accepted format for training
    
    input_parameter:
        jsonl_file: the path of the doccano exported jsonl file with labels
        phase_i   : if the labels are from phase I contracts, set this to True
    
    returns: the labelled data in a spacy ready format
    '''
    TRAIN_DATA = []
    if phase_i:
        label_map = {1: "start_date", 2: "end_date",  4: "seller", 5: "max_budget", 6: "buyer", 
                     7: "sow_num", 8:"sow_name", 13: "agreement_num", 22: "sow_duration"}
    else:
        label_map = {14: "start_date", 15: "end_date", 16: "seller", 17: "max_budget", 18: "buyer",
                     19: "sow_num", 20: "sow_name", 21: "agreement_num", 23: "sow_duration"}
        
    with open(jsonl_file, 'r') as json_file:
        for line in json_file:
            line_ = json.loads(line)
            if line_['annotations']:
                if phase_i:
                    TRAIN_DATA.append((line_['text'], 
                                       {"entities": [(d['start_offset'], d['end_offset'], label_map[d['label']]) 
                                                     for d in line_['annotations'] if d['label'] not in [24]]}
                                        ))
                else:
                    TRAIN_DATA.append((line_['text'], 
                                       {"entities": [(d['start_offset'], d['end_offset'], label_map[d['label']]) 
                                                     for d in line_['annotations'] if d['label'] not in [8]]}
                                        ))
    return TRAIN_DATA