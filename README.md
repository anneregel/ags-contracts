# README #


### How to get started using docker###

* clone this repo
* docker build . -t ags-contracts
* docker run -p 5000:5000 ags-contracts
* go to localhost:5000/upload and upload a contract in docx format

### How to get started without Docker###

* clone this repo
* pip install lexnlp==1.8.0
* pip install -r requirements.txt 
* python app.py or python3 app.py 
* go to localhost:5000/upload and upload a contract in docx/pdf format


### Contact ###

* Pratik Baral
* Other members of DSIA